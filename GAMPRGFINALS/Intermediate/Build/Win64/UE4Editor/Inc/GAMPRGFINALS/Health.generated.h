// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRGFINALS_Health_generated_h
#error "Health.generated.h already included, missing '#pragma once' in Health.h"
#endif
#define GAMPRGFINALS_Health_generated_h

#define GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(exectakeDamage) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_damage); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->takeDamage(Z_Param_damage); \
		P_NATIVE_END; \
	}


#define GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(exectakeDamage) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_damage); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->takeDamage(Z_Param_damage); \
		P_NATIVE_END; \
	}


#define GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHealth(); \
	friend GAMPRGFINALS_API class UClass* Z_Construct_UClass_UHealth(); \
public: \
	DECLARE_CLASS(UHealth, UActorComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAMPRGFINALS"), NO_API) \
	DECLARE_SERIALIZER(UHealth) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUHealth(); \
	friend GAMPRGFINALS_API class UClass* Z_Construct_UClass_UHealth(); \
public: \
	DECLARE_CLASS(UHealth, UActorComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAMPRGFINALS"), NO_API) \
	DECLARE_SERIALIZER(UHealth) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHealth(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHealth) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealth); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealth); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealth(UHealth&&); \
	NO_API UHealth(const UHealth&); \
public:


#define GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealth(UHealth&&); \
	NO_API UHealth(const UHealth&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealth); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealth); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHealth)


#define GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_PRIVATE_PROPERTY_OFFSET
#define GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_10_PROLOG
#define GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_PRIVATE_PROPERTY_OFFSET \
	GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_RPC_WRAPPERS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_INCLASS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_PRIVATE_PROPERTY_OFFSET \
	GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_INCLASS_NO_PURE_DECLS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Health_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPRGFINALS_Source_GAMPRGFINALS_Health_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
