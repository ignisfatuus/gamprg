// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Health.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHealth() {}
// Cross Module References
	GAMPRGFINALS_API UClass* Z_Construct_UClass_UHealth_NoRegister();
	GAMPRGFINALS_API UClass* Z_Construct_UClass_UHealth();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_GAMPRGFINALS();
	GAMPRGFINALS_API UFunction* Z_Construct_UFunction_UHealth_takeDamage();
// End Cross Module References
	void UHealth::StaticRegisterNativesUHealth()
	{
		UClass* Class = UHealth::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "takeDamage", &UHealth::exectakeDamage },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_UHealth_takeDamage()
	{
		struct Health_eventtakeDamage_Parms
		{
			int32 damage;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_damage = { UE4CodeGen_Private::EPropertyClass::Int, "damage", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Health_eventtakeDamage_Parms, damage), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_damage,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Health" },
				{ "ModuleRelativePath", "Health.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_UHealth, "takeDamage", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(Health_eventtakeDamage_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UHealth_NoRegister()
	{
		return UHealth::StaticClass();
	}
	UClass* Z_Construct_UClass_UHealth()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_UActorComponent,
				(UObject* (*)())Z_Construct_UPackage__Script_GAMPRGFINALS,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_UHealth_takeDamage, "takeDamage" }, // 3798691896
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "BlueprintSpawnableComponent", "" },
				{ "ClassGroupNames", "Custom" },
				{ "IncludePath", "Health.h" },
				{ "ModuleRelativePath", "Health.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_currentHealth_MetaData[] = {
				{ "Category", "Health" },
				{ "ModuleRelativePath", "Health.h" },
			};
#endif
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_currentHealth = { UE4CodeGen_Private::EPropertyClass::Int, "currentHealth", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(UHealth, currentHealth), METADATA_PARAMS(NewProp_currentHealth_MetaData, ARRAY_COUNT(NewProp_currentHealth_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_maxHealth_MetaData[] = {
				{ "Category", "Health" },
				{ "ModuleRelativePath", "Health.h" },
			};
#endif
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_maxHealth = { UE4CodeGen_Private::EPropertyClass::Int, "maxHealth", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(UHealth, maxHealth), METADATA_PARAMS(NewProp_maxHealth_MetaData, ARRAY_COUNT(NewProp_maxHealth_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_currentHealth,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_maxHealth,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<UHealth>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&UHealth::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00B00080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHealth, 4177734392);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHealth(Z_Construct_UClass_UHealth, &UHealth::StaticClass, TEXT("/Script/GAMPRGFINALS"), TEXT("UHealth"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHealth);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
