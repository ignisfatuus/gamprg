// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef GAMPRGFINALS_GAMPRGFINALSProjectile_generated_h
#error "GAMPRGFINALSProjectile.generated.h already included, missing '#pragma once' in GAMPRGFINALSProjectile.h"
#endif
#define GAMPRGFINALS_GAMPRGFINALSProjectile_generated_h

#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAMPRGFINALSProjectile(); \
	friend GAMPRGFINALS_API class UClass* Z_Construct_UClass_AGAMPRGFINALSProjectile(); \
public: \
	DECLARE_CLASS(AGAMPRGFINALSProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAMPRGFINALS"), NO_API) \
	DECLARE_SERIALIZER(AGAMPRGFINALSProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGAMPRGFINALSProjectile(); \
	friend GAMPRGFINALS_API class UClass* Z_Construct_UClass_AGAMPRGFINALSProjectile(); \
public: \
	DECLARE_CLASS(AGAMPRGFINALSProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAMPRGFINALS"), NO_API) \
	DECLARE_SERIALIZER(AGAMPRGFINALSProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAMPRGFINALSProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAMPRGFINALSProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAMPRGFINALSProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAMPRGFINALSProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAMPRGFINALSProjectile(AGAMPRGFINALSProjectile&&); \
	NO_API AGAMPRGFINALSProjectile(const AGAMPRGFINALSProjectile&); \
public:


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAMPRGFINALSProjectile(AGAMPRGFINALSProjectile&&); \
	NO_API AGAMPRGFINALSProjectile(const AGAMPRGFINALSProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAMPRGFINALSProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAMPRGFINALSProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGAMPRGFINALSProjectile)


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(AGAMPRGFINALSProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(AGAMPRGFINALSProjectile, ProjectileMovement); }


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_9_PROLOG
#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_RPC_WRAPPERS \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_INCLASS \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_INCLASS_NO_PURE_DECLS \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
