// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRGFINALS_Unit_generated_h
#error "Unit.generated.h already included, missing '#pragma once' in Unit.h"
#endif
#define GAMPRGFINALS_Unit_generated_h

#define GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_RPC_WRAPPERS
#define GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUnit(); \
	friend GAMPRGFINALS_API class UClass* Z_Construct_UClass_AUnit(); \
public: \
	DECLARE_CLASS(AUnit, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAMPRGFINALS"), NO_API) \
	DECLARE_SERIALIZER(AUnit) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAUnit(); \
	friend GAMPRGFINALS_API class UClass* Z_Construct_UClass_AUnit(); \
public: \
	DECLARE_CLASS(AUnit, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAMPRGFINALS"), NO_API) \
	DECLARE_SERIALIZER(AUnit) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnit(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnit) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnit); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnit); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnit(AUnit&&); \
	NO_API AUnit(const AUnit&); \
public:


#define GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnit(AUnit&&); \
	NO_API AUnit(const AUnit&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnit); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnit); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AUnit)


#define GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_PRIVATE_PROPERTY_OFFSET
#define GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_9_PROLOG
#define GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_RPC_WRAPPERS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_INCLASS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_INCLASS_NO_PURE_DECLS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPRGFINALS_Source_GAMPRGFINALS_Unit_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
