// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "GAMPRGFINALSHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGAMPRGFINALSHUD() {}
// Cross Module References
	GAMPRGFINALS_API UClass* Z_Construct_UClass_AGAMPRGFINALSHUD_NoRegister();
	GAMPRGFINALS_API UClass* Z_Construct_UClass_AGAMPRGFINALSHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_GAMPRGFINALS();
// End Cross Module References
	void AGAMPRGFINALSHUD::StaticRegisterNativesAGAMPRGFINALSHUD()
	{
	}
	UClass* Z_Construct_UClass_AGAMPRGFINALSHUD_NoRegister()
	{
		return AGAMPRGFINALSHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_AGAMPRGFINALSHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AHUD,
				(UObject* (*)())Z_Construct_UPackage__Script_GAMPRGFINALS,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Rendering Actor Input Replication" },
				{ "IncludePath", "GAMPRGFINALSHUD.h" },
				{ "ModuleRelativePath", "GAMPRGFINALSHUD.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AGAMPRGFINALSHUD>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AGAMPRGFINALSHUD::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x0080028Cu,
				nullptr, 0,
				nullptr, 0,
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGAMPRGFINALSHUD, 3363506678);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGAMPRGFINALSHUD(Z_Construct_UClass_AGAMPRGFINALSHUD, &AGAMPRGFINALSHUD::StaticClass, TEXT("/Script/GAMPRGFINALS"), TEXT("AGAMPRGFINALSHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGAMPRGFINALSHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
