// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRGFINALS_Weapons_generated_h
#error "Weapons.generated.h already included, missing '#pragma once' in Weapons.h"
#endif
#define GAMPRGFINALS_Weapons_generated_h

#define GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execgetHasAmmo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getHasAmmo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execcheckAmmo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->checkAmmo(); \
		P_NATIVE_END; \
	}


#define GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execgetHasAmmo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getHasAmmo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execcheckAmmo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->checkAmmo(); \
		P_NATIVE_END; \
	}


#define GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWeapons(); \
	friend GAMPRGFINALS_API class UClass* Z_Construct_UClass_AWeapons(); \
public: \
	DECLARE_CLASS(AWeapons, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAMPRGFINALS"), NO_API) \
	DECLARE_SERIALIZER(AWeapons) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAWeapons(); \
	friend GAMPRGFINALS_API class UClass* Z_Construct_UClass_AWeapons(); \
public: \
	DECLARE_CLASS(AWeapons, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAMPRGFINALS"), NO_API) \
	DECLARE_SERIALIZER(AWeapons) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeapons(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeapons) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeapons); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeapons); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeapons(AWeapons&&); \
	NO_API AWeapons(const AWeapons&); \
public:


#define GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeapons(AWeapons&&); \
	NO_API AWeapons(const AWeapons&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeapons); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeapons); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWeapons)


#define GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_PRIVATE_PROPERTY_OFFSET
#define GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_9_PROLOG
#define GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_RPC_WRAPPERS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_INCLASS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_INCLASS_NO_PURE_DECLS \
	GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPRGFINALS_Source_GAMPRGFINALS_Weapons_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
