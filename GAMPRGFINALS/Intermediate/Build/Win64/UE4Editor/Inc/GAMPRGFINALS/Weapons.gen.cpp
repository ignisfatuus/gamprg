// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Weapons.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeapons() {}
// Cross Module References
	GAMPRGFINALS_API UClass* Z_Construct_UClass_AWeapons_NoRegister();
	GAMPRGFINALS_API UClass* Z_Construct_UClass_AWeapons();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_GAMPRGFINALS();
	GAMPRGFINALS_API UFunction* Z_Construct_UFunction_AWeapons_checkAmmo();
	GAMPRGFINALS_API UFunction* Z_Construct_UFunction_AWeapons_getHasAmmo();
// End Cross Module References
	void AWeapons::StaticRegisterNativesAWeapons()
	{
		UClass* Class = AWeapons::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "checkAmmo", &AWeapons::execcheckAmmo },
			{ "getHasAmmo", &AWeapons::execgetHasAmmo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_AWeapons_checkAmmo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Gun Properties" },
				{ "ModuleRelativePath", "Weapons.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AWeapons, "checkAmmo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AWeapons_getHasAmmo()
	{
		struct Weapons_eventgetHasAmmo_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((Weapons_eventgetHasAmmo_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Weapons_eventgetHasAmmo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "Gun Properties" },
				{ "ModuleRelativePath", "Weapons.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AWeapons, "getHasAmmo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(Weapons_eventgetHasAmmo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AWeapons_NoRegister()
	{
		return AWeapons::StaticClass();
	}
	UClass* Z_Construct_UClass_AWeapons()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_GAMPRGFINALS,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_AWeapons_checkAmmo, "checkAmmo" }, // 3564017150
				{ &Z_Construct_UFunction_AWeapons_getHasAmmo, "getHasAmmo" }, // 1884783283
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "Weapons.h" },
				{ "ModuleRelativePath", "Weapons.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_hasAmmo_MetaData[] = {
				{ "Category", "Gun Properties" },
				{ "ModuleRelativePath", "Weapons.h" },
			};
#endif
			auto NewProp_hasAmmo_SetBit = [](void* Obj){ ((AWeapons*)Obj)->hasAmmo = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_hasAmmo = { UE4CodeGen_Private::EPropertyClass::Bool, "hasAmmo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AWeapons), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_hasAmmo_SetBit)>::SetBit, METADATA_PARAMS(NewProp_hasAmmo_MetaData, ARRAY_COUNT(NewProp_hasAmmo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_currentAmmo_MetaData[] = {
				{ "Category", "Gun Properties" },
				{ "ModuleRelativePath", "Weapons.h" },
			};
#endif
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_currentAmmo = { UE4CodeGen_Private::EPropertyClass::Int, "currentAmmo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(AWeapons, currentAmmo), METADATA_PARAMS(NewProp_currentAmmo_MetaData, ARRAY_COUNT(NewProp_currentAmmo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fireRate_MetaData[] = {
				{ "Category", "Gun Properties" },
				{ "ModuleRelativePath", "Weapons.h" },
			};
#endif
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_fireRate = { UE4CodeGen_Private::EPropertyClass::Int, "fireRate", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(AWeapons, fireRate), METADATA_PARAMS(NewProp_fireRate_MetaData, ARRAY_COUNT(NewProp_fireRate_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_range_MetaData[] = {
				{ "Category", "Gun Properties" },
				{ "ModuleRelativePath", "Weapons.h" },
			};
#endif
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_range = { UE4CodeGen_Private::EPropertyClass::Int, "range", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(AWeapons, range), METADATA_PARAMS(NewProp_range_MetaData, ARRAY_COUNT(NewProp_range_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_hasAmmo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_currentAmmo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_fireRate,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_range,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AWeapons>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AWeapons::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWeapons, 1403909787);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWeapons(Z_Construct_UClass_AWeapons, &AWeapons::StaticClass, TEXT("/Script/GAMPRGFINALS"), TEXT("AWeapons"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWeapons);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
