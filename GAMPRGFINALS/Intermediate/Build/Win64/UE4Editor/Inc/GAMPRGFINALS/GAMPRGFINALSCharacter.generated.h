// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRGFINALS_GAMPRGFINALSCharacter_generated_h
#error "GAMPRGFINALSCharacter.generated.h already included, missing '#pragma once' in GAMPRGFINALSCharacter.h"
#endif
#define GAMPRGFINALS_GAMPRGFINALSCharacter_generated_h

#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_RPC_WRAPPERS
#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAMPRGFINALSCharacter(); \
	friend GAMPRGFINALS_API class UClass* Z_Construct_UClass_AGAMPRGFINALSCharacter(); \
public: \
	DECLARE_CLASS(AGAMPRGFINALSCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAMPRGFINALS"), NO_API) \
	DECLARE_SERIALIZER(AGAMPRGFINALSCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAGAMPRGFINALSCharacter(); \
	friend GAMPRGFINALS_API class UClass* Z_Construct_UClass_AGAMPRGFINALSCharacter(); \
public: \
	DECLARE_CLASS(AGAMPRGFINALSCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GAMPRGFINALS"), NO_API) \
	DECLARE_SERIALIZER(AGAMPRGFINALSCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAMPRGFINALSCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAMPRGFINALSCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAMPRGFINALSCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAMPRGFINALSCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAMPRGFINALSCharacter(AGAMPRGFINALSCharacter&&); \
	NO_API AGAMPRGFINALSCharacter(const AGAMPRGFINALSCharacter&); \
public:


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAMPRGFINALSCharacter(AGAMPRGFINALSCharacter&&); \
	NO_API AGAMPRGFINALSCharacter(const AGAMPRGFINALSCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAMPRGFINALSCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAMPRGFINALSCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGAMPRGFINALSCharacter)


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AGAMPRGFINALSCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AGAMPRGFINALSCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AGAMPRGFINALSCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AGAMPRGFINALSCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AGAMPRGFINALSCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AGAMPRGFINALSCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AGAMPRGFINALSCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AGAMPRGFINALSCharacter, L_MotionController); }


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_11_PROLOG
#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_RPC_WRAPPERS \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_INCLASS \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_INCLASS_NO_PURE_DECLS \
	GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPRGFINALS_Source_GAMPRGFINALS_GAMPRGFINALSCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
