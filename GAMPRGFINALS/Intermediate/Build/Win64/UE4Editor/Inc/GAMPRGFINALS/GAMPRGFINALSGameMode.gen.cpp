// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "GAMPRGFINALSGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGAMPRGFINALSGameMode() {}
// Cross Module References
	GAMPRGFINALS_API UClass* Z_Construct_UClass_AGAMPRGFINALSGameMode_NoRegister();
	GAMPRGFINALS_API UClass* Z_Construct_UClass_AGAMPRGFINALSGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_GAMPRGFINALS();
// End Cross Module References
	void AGAMPRGFINALSGameMode::StaticRegisterNativesAGAMPRGFINALSGameMode()
	{
	}
	UClass* Z_Construct_UClass_AGAMPRGFINALSGameMode_NoRegister()
	{
		return AGAMPRGFINALSGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_AGAMPRGFINALSGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_GAMPRGFINALS,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "GAMPRGFINALSGameMode.h" },
				{ "ModuleRelativePath", "GAMPRGFINALSGameMode.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AGAMPRGFINALSGameMode>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AGAMPRGFINALSGameMode::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00880288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGAMPRGFINALSGameMode, 3674980770);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGAMPRGFINALSGameMode(Z_Construct_UClass_AGAMPRGFINALSGameMode, &AGAMPRGFINALSGameMode::StaticClass, TEXT("/Script/GAMPRGFINALS"), TEXT("AGAMPRGFINALSGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGAMPRGFINALSGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
