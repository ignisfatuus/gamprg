// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "GAMPRGFINALSGameMode.h"
#include "GAMPRGFINALSHUD.h"
#include "GAMPRGFINALSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGAMPRGFINALSGameMode::AGAMPRGFINALSGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AGAMPRGFINALSHUD::StaticClass();
}
