// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapons.generated.h"

UCLASS()
class GAMPRGFINALS_API AWeapons : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapons();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, Category = "Gun Properties")
		int32 range;
	UPROPERTY(EditAnywhere, Category = "Gun Properties")
		int32 fireRate;
	UPROPERTY(EditAnywhere, Category = "Gun Properties")
		int32 currentAmmo;
	UPROPERTY(EditAnywhere, Category = "Gun Properties")
		bool hasAmmo;

	UFUNCTION(BlueprintCallable, Category = "Gun Properties")
		void checkAmmo();
	UFUNCTION(BlueprintCallable, Category = "Gun Properties")
		bool getHasAmmo();
	
};
